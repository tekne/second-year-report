#import "report-style.typ": *

#show: doc => report(
    title: "Second Year Report",
    authors: (
        (
            name: "Jad Elkhaleq Ghalayini",
            email: "jeg74@cl.cam.ac.uk"
        ),
    ),
    doc
)

= Progress Report

#let ert = $λ_(sans("ert"))$

In my first year report, I originally planned to produce 3 core contributions:
- A system of explicit refinement types over the simply-typed lambda calculus, #ert.
- An RVSDG-based compiler intermediate representation with a categorical semantics, `isotope`
- A system of explicit refinement types over `isotope`, `nucleus`
My "Plan A" was to extend this by making `isotope` and `nucleus` concurrent; my "Plan B" was to instead explore refinement-guided optimizations and refinement techniques using `nucleus`. In total, I planned to publish 3-5 papers over the course of my PhD.

My progress has been a little slow at first, but now seems to be picking up the pace as I gain a better idea of the concepts and techniques underlying the field:

-  At the time of submitting my first year report, the #ert paper, _Explicit Refinement Types_ had been submitted to OOPSLA'23; unfortunately, it was rejected. However, we resubmitted it to ICFP'23 with minor revisions, and it has now been accepted. We plan to extend #ert to support higher-order logic as part of a new paper, _Adding Nothing to HOL_, which we aim to publish in ICFP'24.

- Work on the `isotope` paper, _Premonoidal Semantics for RVSDGs_, has been ongoing: we have managed to design an intermediate representation with a categorical semantics based on premonoidal categories, and have proved basic metatheoretic results (e.g. congruence, semantic substitution). We have also defined an efficient function to convert our intermediate representation to SSA form, and proved its soundness. 

  We realized that to make `isotope` into a compelling paper, we had to show its categorical semantics was capable of making a difficult problem easier to reason about; hence, we chose to give `isotope` a concurrent semantics supporting weak memory from the get-go to demonstrate this was possible. We explored many different avenues and potential techniques, settling on a pomset-based semantics for the relatively simple SPARC/x86 TSO memory model from @den-sparc; unfortunately, we ran into many difficulties trying to adapt this model to our semantics (as well as a few previous models we tried, e.g. Pomsets with Predicate Transformers @pwt), and were not able to make the POPL'24 deadline; we are currently aiming to submit to OOPSLA'24 or PLDI'24.

My current plan is to continue work on _Premonoidal Semantics for RVSDGs_ and _Adding Nothing to HOL_, with the former being my priority (since #ert is already usable as-is). I then plan to explore adding support for explicit refinement types to `isotope` using the techniques in _Functors are Type Refinement Systems_ @ftrs. While I plan to support at least one nontrivial `isotope` model (one of the main advantages of giving `isotope` a categorical semantics is that it is easy to swap out the underlying model without re-doing much work), there are many open questions involved in trying to make my refinement type system support concurrency, but doing so would be ideal.

Overall, therefore, I plan to have completed 4 papers by the end of my PhD:
- _Explicit Refinement Types_ in ICFP'23 (available at #link("https://tekne.dev/ert.pdf"))
- _Premonoidal Semantics for RVSDGs_ in PLDI'24 or OOPSLA'24
- _Adding Nothing to HOL_ in ICFP'24
- A paper on adding explicit refinement types to `isotope` in POPL'25

So far, I've also given the following presentations:
- _Typechecking up to Congruence_ at WITS'22 (slides: #link("https://tinyurl.com/wits22-slides"))
- _Explicit Refinement Types_ at a departmental seminar (slides: #link("https://tinyurl.com/ert-slides-cam"))

#pagebreak()

= Thesis Outline

== Chapter 1: Overview of `isotope`

=== Planned Content

I plan to begin this chapter with an overview of the design of `isotope`, comparing and contrasting the RVSDG with traditional intermediate representations based on SSA form. I then plan to given an overview of the textual and graphical syntax of `isotope`, along with various examples of `isotope` programs in each format, followed by a formal overview of the typing rules and some basic syntactic metatheory (e.g.  substitution). I then plan to give a formal description of an efficient interconversion algorithm between `isotope` and SSA form.

=== Current Status

I have written up an introduction to the syntax of `isotope` along with various example programs, and have worked out a formal grammar, typing rules, and proofs of various metatheoretic properties. I have also described an algorithm for interconverting between `isotope` and SSA programs. I still need to formally describe the graphical representation of `isotope` programs (though I have already informally worked it out), and put everything into the structure of a thesis chapter.

== Chapter 2: Premonoidal Categories and State-Splitting

=== Planned Content

In this chapter, I plan to give a basic overview of premonoidal and monoidal categories, followed by a description of the construction in @premonoidal-string-diagrams for using string diagrams to reason about premonoidal categories, which in the setting of `isotope` I call "state threading." I then plan to go over a generalization of this construction to support "state splitting," which is useful for constructing models of `isotope` which support structured single-threaded concurrency, as in the original RVSDG paper @rvsdg. I then plan to introduce the concept of an _iterative category_,  which we use to model potentially nonterminating control flow, and describe how the state splitting construction can be generalized to this setting.

=== Current Status

The research for this chapter is currently in progress: I have an idea of how to perform the state splitting construction in the finite setting, but am still exploring various candidate generalizations to the fully general iterative setting. In the worst case, however, the state threading construction is still enough to construct various interesting `isotope` models, including many with support for concurrency.

== Chapter 3: Premonoidal Semantics for RVSDGs

=== Planned Content

I plan to continue the previous chapter by giving a categorical semantics for textual `isotope` programs using premonoidal iterative categories, followed by a proof that this semantics has the desired metatheoretic properties (e.g. substitution and congruence) and a proof that the SSA algorithm given in the first chapter is correct in any `isotope` model. I then plan to note that, to support graphical `isotope` programs naively, a _monoidal_ iterative category is required; by using the "state threading" and "state splitting" constructions can be used to allow graphical representation of programs in the more general premonoidal models.

I then wish to construct some simple _monoidal_ models of `isotope` (namely, the Kliesli category of the nontermination monad and nondeterminism monads), and show how these can be generalized to the _premonoidal_ setting by adding support for a simple side-effect (reading and writing pointers in a flat memory model). Finally, I plan to instantiate the state threading and state splitting constructions for these simple cases to build intuition in the reader.

=== Current Status

I have written down a categorical semantics for textual isotope programs and proved various metatheoretic properties for it. I have also written up a draft of a proof of the correctness of the SSA algorithm. Both monoidal categories given can be trivially shown to be `isotope` models, but I have yet to construct their side-effecting versions, and of course my work on state-splitting is not yet complete either.

== Chapter 4: Premonoidal Semantics for Weak Memory Concurrency

=== Planned Content

In this chapter, I plan to construct a more complicated `isotope` model which supports weak memory concurrency modelled after SPARC/x86 TSO, based on the pomset model in @den-sparc. I then plan to give an overview of how the model can be generalized to support optimizations such as elimination of redundant reads and writes, which are not supported by a naive model, and how to show such extensions are sound. I also plan to go over how this model is partially formalized in Lean 4.

=== Current Status

I have begun a Lean 4 formalization of a modified version of the pomset model in @den-sparc and have made very good progress, with the goal of proving various properties of the model not explicitly stated in the paper but necessary for building a valid `isotope` model over it. I chose to formalize this work in a theorem prover due to its high complexity, leading to a high potential for mistakes (I found 3 serious mistakes in my pen-and-paper formalization already, requiring me to change a few definitions slightly). While I expect to be able to build the current pomset model up into a full `isotope` model (though I am currently struggling with proving that the category I have constructed is iterative), supporting elimination of redundant relaxed reads and writes is still future work.

== Chapter 5: RVSDG optimizations

=== Planned Content

In this chapter, I plan to go over the high-level implementation of some basic optimization algorithms on RVSDGs, such as:
- Strength reduction and peephole optimization
- Loop fusion and hoisting
- `mem2reg`/`sroa`
- Sparse conditional constant propagation
I then plan to prove the correctness of each optimization in a certain class of `isotope` models via `isotope`'s categorical semantics. I then plan to repeat this for E-graph based rewriting optimizations on `isotope`'s syntax.

=== Current Status

This research is still in the conceptual stage, though I have done some experiments on rewriting `isotope` programs using the `egg` E-graph library @2021-egg.

== Chapter 6: Implementation, Compilation, and Optimization of `isotope` Programs

=== Planned Content

In this chapter, I plan to go over the practical details of implementing, optimizing, and compiling `isotope` programs, and
describe the Rust implementation of `isotope`. In particular, I aim to describe:
- The in-memory representation of `isotope` programs, as well as serializing and deserializing `isotope` programs from a binary format using Rust's `serde` library.
- Compilation of `isotope` to LLVM (and potentially Cranelift and/or WASM)
- Testing and benchmarking of `isotope` and `isotope` optimizations

=== Current Status

I've currently written a parser for `isotope` in Rust and done various experiments with different internal representations, as well as E-graph based optimization, but the implementation is still in its early stage as it's not strictly necessary for the `isotope` paper (due to its theoretical focus).

== Chapter 7: Explicit Refinement Types

=== Planned Content

I plan for this chapter to essentially be a summarized version of _Explicit Refinement Types_, going over the motivation, syntax, and typing rules of #ert with a variety of examples, followed by a brief formal presentation of the typing rules, erasure to the simply-typed lambda calculus, denotational semantics, and metatheoretic results, and, finally, a description of the Lean 4 formalization. Hopefully, this will present the higher-order version of #ert, but in either case the presentation should be roughly the same.

=== Current Status

The _Explicit Refinement Types_ paper and associated Lean 4 formalization are both complete, but _Adding Nothing to HOL_ is still in its early stages. Summarizing the paper into just a chapter may also prove challenging, since it is quite long.

== Chapter 8: Refinement Types for `isotope`

=== Planned Content

In this chapter, I plan to begin putting everything together by describing a system of explicit refinement types for `isotope`. In particular, after going over syntax and examples, I plan to give a formal description of the typing rules and syntactic metatheory, as well as an erasure algorithm from refined `isotope` programs to their regular counterparts.

=== Current Status

This research is still in the conceptual stage.

== Chapter 9: Semantics of `isotope` Refinement Types

=== Planned Content

I plan to continue the previous chapter by giving a semantics for `isotope`'s refinement types using the techniques in _Functors are Type Refinement Systems_ @ftrs, followed by proofs of desirable metatheoretic properties such as soundness of the erasure procedure and semantic substitution.

=== Current Status

This research is still in the conceptual stage.

= Timetable

/ October 2023:  Finish _Premonoidal Semantics for RVSDGs_ for submission to OOPSLA'24 or PLDI'24, corresponding to chapters 1-5, and potentially part of chapter 6; start writing chapters 1-5.
/ February 2024: Finish _Adding Nothing to HOL_ for submission to ICFP'24, corresponding to chapter 7; start writing chapter 7.
/ July 2024: Finish `isotope` refinement types paper for submission to POPL'25, corresponding to chapters 8-9; start writing chapters 8-9.
/ September 2024: Finish first draft of thesis, mainly using parts of previously submitted papers
/ December 2024: Revise first draft of thesis

#pagebreak()
#bibliography("references.bib")