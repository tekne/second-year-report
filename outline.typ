= Second Year PhD Report

== Research Summary

#let ert = $λ_(sans("ert"))$

- Finished ICFP'23 paper _Explicit Refinement Types_
    - Researched:
        - Categorical semantics of refinement types (not used that much)
        - Logical relations
        - Denotational semantics
        - Using Lean 4 for nontrivial projects
    - Done:
        - Formalization of #ert in $~17$k lines of Lean 4, including:
            - Syntax
            - Typing
            - Weakening + Substitution + Regularity
            - Simply typed lambda calculus (w. all of above)
            - Denotational semantics for STLC
            - Semantic substitution, soundness of STLC
            - STLC erasure for #ert
            - Soundness, substitution for erasure
            - Soundness, substitution for induced denotational semantics for #ert
    - Future work:
        - _Adding Nothing to HOL_
- Work on OOPSLA'24? paper _Categorical Semantics for Compiler Intermediate Representations_:
    - Researched:
        - Categorical semantics
        - Iteration algebra (Elgot algebra? What do I call this?)
        - RVSDG, SSA form (??)
        - Weak memory, SPARC/Intel TSO
    - Done:
        - Grammar and parser for `isotope`
        - Categorical semantics for textual `isotope`
            - Proof of substitution, weakening, etc
            - Proof of rewriting theorem (go fix)
    - In progress:
        - `isotope` to SSA interconversion
        - `isotope` graphical syntax
        - `isotope` model for SC formalized in Lean 4
        - `isotope` model for SPARC TSO formalized in Lean 4
    - Preliminary:
        - State-splitting semantics for single-threaded concurrency
        - Actual compiler toolchain implementation
- Preliminary work on:
    - _Adding Nothing to HOL_: extending _Explicit Refinement Types_ with higher-order logic

== Other Work

- Teaching: 
    - Types
    - Optimizing Compilers
    - Hoare Logic and Model Checking
    - Operating Systems
    - Complexity
- Volunteering:
    - Supervisors' workshop
    - Chairing MSc presentations
- Language learning: French, Chinese, and Arabic

== Thesis Schedule

Title: _Categorical Semantics and Refinement Types for Compiler Intermediate Representations_

Outline:

- Basic Explicit Refinement Types (research completed but not written)
- Higher Explicit Refinement Types (research in progress)
- Premonoidal semantics for RVSDG-like intermediate representations (research in progress)
- Premonoidal semantics for weak memory (research in progress)
- Implementation of RVSDGs (research in progress)
- Categorical semantics of RVSDG optimization (research in progress)
- Implementation of RVSDG optimization (research not started)
- Refinement types for RVSDGs with categorical semantics (research not started)
- Refinement-guided optimization for RVSDGs (research not started)

== Remaining Work

- Finish _Categorical Semantics for Compiler Intermediate Representations_
- Finish _Adding Nothing to HOL_
- Go refine `isotope`
- Face the void