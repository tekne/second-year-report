#let report(
  title: none,
  authors: (),
  doc,
) = {
    // Figures
    show figure: it => {
        show: pad.with(x: 23pt)
        set align(center)

        v(12.5pt, weak: true)

        // Display the figure's body.
        it.body

        // Display the figure's caption.
        if it.has("caption") {
        // Gap defaults to 17pt.
        v(if it.has("gap") { it.gap } else { 17pt }, weak: true)
        smallcaps[Figure]
        if it.numbering != none {
            [ #counter(figure).display(it.numbering)]
        }
        [. ]
        it.caption
        }

        v(15pt, weak: true)
    }

    if type(authors) == "dictionary" {
        authors = (authors,)
    }
    let title-page = page([
        #set align(top + center)
        #v(2.5cm)
        #text(2em)[*#title*]
        #v(2.5cm)
        #image("uc-black-white.svg", width: 50%)
        #v(10cm)
        #{
            for author in authors {
                grid(
                    gutter: 10pt,
                    author.name,
                    if "email" in author [
                        #link("mailto:" + author.email)
                    ]
                )
            }
        }
    ])
    [
    #title-page
    #pagebreak()

    #set page(
        numbering: "1"
    )

    #doc
    ]
}